#!/usr/bin/with-contenv bash

function ts {
  echo [`date '+%b %d %X'`]
}

#-----------------------------------------------------------------------------------------------------------------------

function initialize_configuration {
  echo "$(ts) Creating /config/filebot.conf.new"
  cp /filebot/filebot.conf /config/filebot.conf.new

  if [ ! -f /config/filebot.conf ]
  then
    echo "$(ts) Creating /config/filebot.conf"
    cp /filebot/filebot.conf /config/filebot.conf
    chmod a+w /config/filebot.conf
  fi

  # Clean up carriage returns
  tr -d '\r' < /config/filebot.conf > /tmp/filebot.conf
  mv -f /tmp/filebot.conf /config/filebot.conf

  # Create filebot.sh unless there is already one
  if [ ! -f /config/filebot.sh ]
  then
    echo "$(ts) Creating /config/filebot.sh and exiting"
    cp /filebot/filebot.sh /config/filebot.sh
    exit 1
  fi
}

#-----------------------------------------------------------------------------------------------------------------------

function create_conf_and_sh_files {
  # Create the config file for monitor.py
  cat <<"EOF" > /filebot/filebot.conf
if [[ -z "$INPUT_DIR" ]]
then
  INPUT_DIR=/input
fi

if [[ -z "$OUTPUT_DIR" ]]
then
  OUTPUT_DIR=/output
fi

EOF

  tr -d '\r' < /config/filebot.conf >> /filebot/filebot.conf

  # Literal $INPUT_DIR
  cat <<"EOF" >> /filebot/filebot.conf

WATCH_DIR="$INPUT_DIR"
EOF

  # Interpolate $USER_ID, $GROUP_ID, and $UMASK
  cat <<EOF >> /filebot/filebot.conf

COMMAND="bash /filebot/filebot.sh"

IGNORE_EVENTS_WHILE_COMMAND_IS_RUNNING=0

USER_ID=$USER_ID
GROUP_ID=$GROUP_ID
UMASK=$UMASK
EOF

  # Strip \r from the user-provided filebot.sh
  tr -d '\r' < /config/filebot.sh > /filebot/filebot.sh
  chmod a+wx /filebot/filebot.sh
}

#-----------------------------------------------------------------------------------------------------------------------

function validate_configuration {
  . /filebot/filebot.conf

  if [[ ! -d "$INPUT_DIR" ]]; then
    echo "$(ts) INPUT_DIR=$INPUT_DIR does not exist"
    exit 1
  fi

  if [[ ! -d "$OUTPUT_DIR" ]]; then
    echo "$(ts) OUTPUT_DIR=$OUTPUT_DIR does not exist"
    exit 1
  fi

  if find "$INPUT_DIR" -inum $(stat -c '%i' "$OUTPUT_DIR") 2>/dev/null | grep . 1>/dev/null 2>&1; then
    echo "$(ts) OUTPUT_DIR=$OUTPUT_DIR can not be a subdirectory of INPUT_DIR=$INPUT_DIR"
    exit 1
  fi
}

#-----------------------------------------------------------------------------------------------------------------------

function setup_opensubtitles_account {
  . /filebot/filebot.conf

  if [ "$OPENSUBTITLES_USER" != "" ]; then
    echo "$(ts) Configuring for OpenSubtitles user \"$OPENSUBTITLES_USER\""
    echo -en "$OPENSUBTITLES_USER\n$OPENSUBTITLES_PASSWORD\n" | /files/runas.sh $USER_ID $GROUP_ID $UMASK filebot -script fn:configure
  else
    echo "$(ts) No OpenSubtitles user set. Skipping setup..."
  fi
}

#-----------------------------------------------------------------------------------------------------------------------

echo "$(ts) Starting FileBot container"

initialize_configuration

create_conf_and_sh_files

validate_configuration

setup_opensubtitles_account

# permissions
chmod a+rwX /filebot/filebot.conf && \
chmod +x /filebot/{filebot.sh,pre-run.sh}
chmod +x /etc/services.d/filebot/run
